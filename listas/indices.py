cursos = ["python", "djago", "flask", "c", "c++", "c#", "java", "php"]
curso = cursos[0]
print(curso)

#Se puede recorrer la lista a la inversa con indice negativos
curso = cursos[-1]
print(curso)

sub = cursos[0:3]
print(sub)

# Sub lista con saltos de 2 en dos
sub = cursos[1:7:2]
print(sub)